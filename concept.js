// nodejs is run time env built on chrome's v8 engine

// npm and npx are pacakge management tool used to setup and maintain JS project

// (npm can be replaced by yarn)


// to initiate project
// command

// npm init ==> initilize project creating a pacakge.json file

// package.json  file ==> project's introductory file
// package-lock.json (yarn.lock) ==> files to lock versiion of dependent pacakges (all the packages of dependency tree)
// node_modules ==> folder to store installed packages

// npmjs.com ==> global repository to hold javascript packages
// to installl package
// npm install <pacakge_name>  ==> it will be installed in your desired folder inside node_modules folder
// npm install <pacakge_name> --global ==> it will be installed in your machine
// npm install <pacakge_name> --save-dev ==>it will be listed in devDependencies in package.json

// to uninstall package

// npm uninstall <package_name>

// protocol ==> set of rules

// http protocol
// http verb(method)
// GET,PUT,POST,DELETE,HEAD,OPTIONS,PATCH
// http url 

// http ==> request 
// http ===> response

// http status code
// 100 information
// 200 ==> success
// 300 ==> ridirection
// 400 ==> application error
// 500 ==> server

// to establish communication we need endpoint

// endpoint===> combination of http method and url

// to creata a server we will import http module

// tier architecture
// client-server
// MVC



// REST API


// API 
// Application programming interface
// http
// endpoint ===> /login POST, /register POST [combination of url and method]


// REST
// Representational State Transfer
// an API is restful if it fullfills following point

// 1. Stateless ===> server will never save client request
// each request to server is independant

// 2. correct use of http verb
// GET ==> data fetch
// POST ==> data send
// PUT/PATCH ==> update
// DELETE ==> delete

// 3. data format must be either JSON or XML
// json

// 4. Caching ==> GET 
// DB call caching ==> 







