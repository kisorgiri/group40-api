// setTimeout() inbuilt method to dealy
// syntax
// setTimeout(function () {
//   console.log('afte one sec')
//  }, 1000)

// console.log('step 1')
// console.log('step 2')
// console.log('step 3')
// setTimeout(() => {
//   console.log('step 4')

// }, 2000)
// console.log('step 5')
// console.log('step 6')

// Problem Identify
// Problem Define
// Solution

// callback 
// callback is a function which is passed as an argument to another function

// function sendMail(data, xyz) { // HOF ==> function accepting function as an parameters
//   console.log('what comes in >>', data)
//   console.log('what comes in xyz >>', xyz)

//   // xyz();

// }

// sendMail('random', function () {

// })

// function passed as an argument is callback function

// [] 
// {}
// string 
// number
// function ==> call

// function buyCycle(model, cb) {
//   console.log('i am at bike shop to get new bike');
//   console.log('shopkeeper told me to wait till evening for desired model');
//   // delay
//   setTimeout(function () {
//     console.log('bike available at shop')
//     console.log('now callback')
//     cb(); //

//   }, 2000)
// }

// console.log('execution starts here');
// console.log('i need a bike')
// buyCycle('all mountain', function () {
//   console.log('callback block executed');
//   // depedant task here
//   console.log('go for ride');
//   console.log('take a picture')
// })
// // independant task
// // non blocking task

// console.log('have a coffee')

// task part
// function askMoney(amt, cb) {
//   console.log('mom told me to wait')
//   setTimeout(() => {
//     cb();
//   }, 2000)
// }

// console.log('i need money to buy something');
// console.log('ask money with mom');

// askMoney(2333, function () {
//   console.log('callback block used to perform depedant task')
// })
// // independant task performing block
// console.log('eat food');
// console.log('go to college')


// execution

// db_connect(data, function () {

// })

// sendMail(data, function () {

// })

// db_insert(data, function () {

// })


// fetchWater(function () {
//   // 
// })
// // 

// setTimeout(function(){

// }, 333)

// callback complete picture

// task part
function db_connect(url,cb){
  // internal work

  // cb('any value') // for error
  cb(null,'any value') //for success
}

db_connect('conxn_url',function(err,done){
    if(err){
      // result with error
      console.log('here in error ')
    }
    else{
      // result with success
      console.log('here in success')
    }
})