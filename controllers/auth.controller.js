const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model')
const uploader = require('./../middlewares/uploader')
const passwordHash = require('password-hash')
const jwt = require('jsonwebtoken');
const config = require('./../configs')
const nodemailer = require('nodemailer');


const sender = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: 'broadwaytest44@gmail.com',
    pass: 'Broadwaytest44!'
  }
})

function prepareEmail(data) {
  return {
    from: 'Group40 IMS', // sender address
    to: "rijalsunam0@gmail.com," + data.email, // list of receivers
    subject: "Forgot Password ✔", // Subject line
    text: "Forgot Password", // plain text body
    html: `
      <div>
      <p>Hi <strong> ${data.name} </strong>,</p>
      <p>We noticed that you are having trouble logging into our system.please use link below to reset your password</p>
      <p><a href="${data.link}">cliclk here to reset your password</a></p>
      <p>Warm Regards</p>
      <p>Group 40 IMS Support Team</p>
      <p>2022</p>
      </div>
    `, // html body
  }
}

function getToken(data) {
  return jwt.sign({
    username: data.username,
    role: data.role,
    _id: data._id
  }, config.JWT_SECRET)
}

const MapUserReq = require('./../helpers/mapUserReq')
router.post('/login', function (req, res, next) {
  if (!req.body.username) {
    return next({
      msg: 'please provide username to continue'
    })
  }
  UserModel
    .findOne({
      $or: [
        {
          username: req.body.username.trim().toLowerCase()
        },
        {
          email: req.body.username
        }
      ]
    })
    .then(function (user) {
      if (!user) {
        return next({
          msg: 'Invalid Username',
          status: 400
        })
      }
      // password verification
      let isMatched = passwordHash.verify(req.body.password, user.password);
      if (!isMatched) {
        return next({
          msg: 'Invalid Password',
          status: 400
        })
      }
      // token generation
      let token = getToken(user)
      res.json({
        token,
        user
      })
    })
    .catch(err => {
      next(err)
    })
})

router.post('/register', uploader.single('image'), function (req, res, next) {
  // if i have req.file or req.files I am certain that file is already uploaded
  console.log('req.body...', req.body)
  console.log('req.file >>>', req.file);
  if (req.fileTypeErr) {
    return next({
      msg: 'Invalid File Format',
      status: 400
    })
  }
  // console.log('req.files >>>', req.files);
  // fileter file type [NOTE ==>server ma upload vaye pachi]
  if (req.file) {
    req.body.image = req.file.filename;
  }

  const newUser = new UserModel({});
  // newUser mongoose object
  const newMappedUser = MapUserReq(req.body, newUser)
  newUser.password = passwordHash.generate(req.body.password)

  newUser.save(function (err, done) {
    if (err) {
      return next(err)
    }
    res.json(done)
  })

})

router.post('/forgot-password', function (req, res, next) {
  UserModel.findOne({
    email: req.body.email
  }, function (err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next({
        msg: "Email Not Registered Yet",
        status: 404
      })
    }

    let passworResetExpiryTime = Date.now() + 1000 * 60 * 60 * 24;
    // if user found now proceed with emails
    let emailData = {
      name: user.username,
      email: user.email,
      link: `${req.headers.origin}/reset_password/${user._id}`
    }

    var emailContent = prepareEmail(emailData);


    user.passwordResetExpiry = passworResetExpiryTime;
    user.save(function (err, done) {
      if (err) {
        return next(err)
      }
      sender.sendMail(emailContent, function (err, done) {
        if (err) {
          return next(err);
        } else {
          res.json(done)
        }
      })
    })
  })

})

router.post('/reset-password/:id', function (req, res, next) {
  let id = req.params.id;

  UserModel.findOne({
    _id: id,
    passwordResetExpiry: {
      $gte: Date.now()
    }
  }, function (err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next({
        msg: "Invalid or Expired Password Reset Link",
        status: 400
      })
    }
    // let now  = Date.now();
    // if(user.passwordResetExpiry <= now){
    //   // proceed
    // }
    // user found now update password
    user.password = passwordHash.generate(req.body.password);
    user.passwordResetExpiry = null;
    user.save(function (err, done) {
      if (err) {
        return next(err);
      }
      res.json(done)
    })
  })
})


module.exports = router;