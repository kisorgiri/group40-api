// 

var fruits = ['apple', 'mango', 'grape', 'orange', 'kiwi', 'apple', 'mango', 'grape', 'orange'];
var abc = ['a', 'a', 'a', 'b']
// task 1 is==>
// write a function to find unique array of values from given array

// includes
console.log('check includes >>', fruits.includes('kiwi'))
console.log('find index >>', fruits.indexOf('lksdjflsda'))
function getUnique(arg) {
  var empt = [];
  arg.forEach(function (item, index) {
    // item or 1st arg is always element of array
    // index or 2nd arg is always index of element of array
    console.log('item >>', item);
    // if (empt.includes(item)) {
    //   console.log('already exists in targeted array')
    // } else {
    //   empt.push(item)

    // }
    // if (!empt.includes(item)) {
    //   empt.push(item)
    // }
    if (empt.indexOf(item) === -1) {
      empt.push(item)
    }
    // push,unshift
  })
  return empt;
}

// // hint is loop
// // forEach 

var res = getUnique(fruits); // unique value of fruits
console.log('res is >>', res)
// var res1 = getUnique(abc)// ['a','b']

// task 2 
// find count of repeted item from given array

function getCount(arg) {
  var count = {};
  arg.forEach(function (item, index) {
    count[item] = (count[item] || 0) + 1;
  })
  return count;
}

var countRes = getCount(fruits)
console.log('count ', countRes)
// expected output 
// {
//   apple: 2,
//     mango: 4,
//       kiwi: 4
// }

var res1 = getCount(abc);
console.log('count 2 >>', res1)
// {
//   a:4,
//   b1
// }