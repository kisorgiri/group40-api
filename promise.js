// function repairMobile(brand, cb) {
//   cb('failed')
//   cb('failed 1')
//   cb(null, 'done')
//   cb(null,'ok')
// //   cb(null,'awesome')
// }

// repairMobile('samsung', function (err, done) {
//   if (err) {
//     console.log('error reparing', err)
//   } else {
//     console.log('success in reparing ', done)
//   }
// })


// promise  ===> promise is an object which holds value of future result
// promise has three different methods
// then, catch, finally

// promise has three different state
// pending ==> when promise is initilized
// onFullfilled ==> success
// onRejection == failure
// settled ==> either success or failure it is termed as settled

// NOTE :-- once promise is settled it will never change its state

// var obj = {
//   name:'kishor',
//   phone:333,
//   email:'jskisor@gmail.com',
//   getPhone:function(){

//   }

// }

// syntax
// var abc = new Promise(function (success, failure) {
//   // success or 1st argument is callback for success
//   // failure or 2nd argument is callback for failure
//   // failure('helo')
//   success('hi')
// })

// // console.log('abc is >>', abc)
// abc
//   .then(function (data) {
//     console.log('success result in then >>', data)
//   })
//   .catch(function (err) {
//     console.log('failure inside catch >>', err)
//   })
// .finally(function () {
//   console.log('finally promise is settled')
// })

function askMoney(amt) {
  console.log('mom told me to wait ')
  return new Promise(function (resolve, reject) {
    setTimeout(() => {
      console.log('mom gets her salary')
      resolve();
      reject();
    }, 1000)
  })
  // lkdjldskjf

}

console.log('promise execution started')

askMoney(3333)
  .then(() => {
    console.log('success =also')
    console.log('here is succcess',hi)
  })
  .catch((err) => {
    console.log('here in failure', err)
  })
