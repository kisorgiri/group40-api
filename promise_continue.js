// task part
function takePhoto(destination) {
  console.log('taking ....');
  return new Promise(function (resolve, reject) {
    setTimeout(() => {
      resolve('done');
    }, 2000)
  })

}

function editPhoto(rawPhoto) {
  return new Promise(function (resolve, reject) {
    console.log('editing ....')
    setTimeout(() => {
      reject('done')
    }, 4000)
  })

}

function upload(platform) {
  return new Promise(function (resolve, reject) {
    console.log('uploading ...')
    setTimeout(() => {
      resolve('done')
    }, 1000);
  })

}


// nested promise
// pormise chaining rule

takePhoto('bkt')
  .then(function (data) {
    console.log('i have photo')
    console.log('now edit ');
    return editPhoto('lkdsjf.png');

  })
  .then(function (data) {
    console.log('success part of edit phtoto', data)
    return upload('fb');
  })
  .then(function (data) {
    console.log('success of upload >>', data)
  })
  .catch(function (err) {
    console.log('inside catch block', err)
  })

  // non blocknig task


